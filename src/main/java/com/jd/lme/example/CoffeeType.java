package com.jd.lme.example;

public enum CoffeeType {
    Americano,
    Cappuccino,
    Latte,
    Mocha
}
