package com.jd.lme.example.favors;

/**
 * 全糖
 */
public class FullSugar implements Favor{
    @Override
    public String desc() {
        return "全糖";
    }
}
