package com.jd.lme.example.favors;

/**
 * 低糖
 */
public class LowSugar implements Favor{
    @Override
    public String desc() {
        return "低糖";
    }
}
