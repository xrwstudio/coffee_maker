package com.jd.lme.example.favors;

/**
 * 无糖
 */
public class NoSugar implements Favor{
    @Override
    public String desc() {
        return "无糖";
    }
}
