package com.jd.lme.example.favors;

public enum FavorEnum {
    No,
    Low,
    Half,
    High,
    Full
}
