package com.jd.lme.example.materials;

/**
 * 奶油
 */
public class Cream implements Material {

    @Override
    public String desc() {
        return "奶油";
    }
}
