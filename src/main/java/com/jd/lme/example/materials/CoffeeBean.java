package com.jd.lme.example.materials;

/**
 * 咖啡豆
 */
public class CoffeeBean implements Material {

    @Override
    public String desc() {
        return "咖啡豆";
    }
}
