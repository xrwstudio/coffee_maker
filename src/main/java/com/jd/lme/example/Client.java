package com.jd.lme.example;

import com.jd.lme.example.favors.FavorEnum;

/**
 * Hello world!
 *
 */
public class Client
{
    public static void main( String[] args )
    {
        CoffeeMaker coffeeMaker = new CoffeeMaker();
        Coffee coffee = coffeeMaker.makeCoffee(CoffeeType.Cappuccino, FavorEnum.Half);
        String desc = coffee.desc();
        System.out.println(desc);
    }
}
