package com.jd.lme.example;

import com.jd.lme.example.builders.Builders;
import com.jd.lme.example.builders.CoffeeBuilder;
import com.jd.lme.example.favors.Favor;
import com.jd.lme.example.favors.FavorEnum;
import com.jd.lme.example.favors.FavorFactory;

public class CoffeeMaker {

    Coffee makeCoffee(CoffeeType coffeeType, FavorEnum favorEnum){
        Favor favor = FavorFactory.createFavor(favorEnum);
        CoffeeBuilder coffeeBuilder = Builders.newBuilder(coffeeType);
        return coffeeBuilder.favor(favor).build();
    }
}
