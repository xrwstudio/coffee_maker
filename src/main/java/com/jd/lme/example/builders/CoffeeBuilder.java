package com.jd.lme.example.builders;

import com.jd.lme.example.Coffee;
import com.jd.lme.example.favors.Favor;

public interface CoffeeBuilder {

    CoffeeBuilder favor(Favor favor);

    Coffee build();
}
