package com.jd.lme.example.builders;

import com.jd.lme.example.Latte;
import com.jd.lme.example.materials.CoffeeBean;
import com.jd.lme.example.materials.Milk;


/**
 * 构建拿铁咖啡
 */
public class LatteCoffeeBuilder extends AbstractCoffeeBuilder{
    public LatteCoffeeBuilder() {
        this.coffee = new Latte();
    }

    @Override
    public void addMaterial() {
        CoffeeBean coffeeBean = new CoffeeBean();
        coffee.add(coffeeBean, 1);
        Milk milk = new Milk();
        coffee.add(milk, 2);
    }
}
