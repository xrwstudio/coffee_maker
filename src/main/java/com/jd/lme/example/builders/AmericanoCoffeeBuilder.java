package com.jd.lme.example.builders;

import com.jd.lme.example.Americano;
import com.jd.lme.example.materials.CoffeeBean;
import com.jd.lme.example.materials.Water;

public class AmericanoCoffeeBuilder extends AbstractCoffeeBuilder{
    public AmericanoCoffeeBuilder() {
        this.coffee = new Americano();
    }

    @Override
    public void addMaterial() {
        System.out.println("步骤二：添加原料");
        CoffeeBean coffeeBean = new CoffeeBean();
        coffee.add(coffeeBean, 1);
        Water water = new Water();
        coffee.add(water, 2);
    }
}
