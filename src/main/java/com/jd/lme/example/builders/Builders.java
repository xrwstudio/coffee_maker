package com.jd.lme.example.builders;

import com.jd.lme.example.CoffeeType;


/**
 * 简单工厂模式
 */
public class Builders {
    public static CoffeeBuilder newBuilder(CoffeeType coffeeType){
        CoffeeBuilder coffeeBuilder;
        switch (coffeeType){
            case Americano:
                coffeeBuilder = new AmericanoCoffeeBuilder();
                break;
            case Latte:
                coffeeBuilder = new LatteCoffeeBuilder();
                break;
            case Cappuccino:
                coffeeBuilder = new CappuccinoCoffeeBuilder();
                break;
            default:
                coffeeBuilder = new AmericanoCoffeeBuilder();
        }
        return coffeeBuilder;
    }
}
