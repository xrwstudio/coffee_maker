package com.jd.lme.example.builders;

import com.jd.lme.example.Cappuccino;
import com.jd.lme.example.materials.CoffeeBean;
import com.jd.lme.example.materials.Milk;
import com.jd.lme.example.materials.MilkFoam;

public class CappuccinoCoffeeBuilder extends AbstractCoffeeBuilder{
    public CappuccinoCoffeeBuilder() {
        this.coffee = new Cappuccino();
    }

    @Override
    protected void addMaterial() {
        System.out.println("步骤二：添加原料");
        Milk milk = new Milk();
        CoffeeBean coffeeBean = new CoffeeBean();
        MilkFoam milkFoam = new MilkFoam();

        coffee.add(coffeeBean, 1);
        coffee.add(milk, 1);
        coffee.add(milkFoam, 1);
    }
}
