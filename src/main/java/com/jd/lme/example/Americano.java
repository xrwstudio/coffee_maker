package com.jd.lme.example;



/**
 * 美式咖啡
 */
public class Americano extends Coffee {

    public Americano() {
    }

    @Override
    public String name() {
        return "美式咖啡";
    }

}
